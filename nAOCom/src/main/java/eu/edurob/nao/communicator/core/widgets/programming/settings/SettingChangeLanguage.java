package eu.edurob.nao.communicator.core.widgets.programming.settings;

import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import eu.edurob.nao.communicator.MainActivity;
import eu.edurob.nao.communicator.R;
import eu.edurob.nao.communicator.network.data.response.DataResponsePackage;
import eu.edurob.nao.communicator.network.interfaces.NetworkDataReceivedListener;

public class SettingChangeLanguage extends AbstractSettingsContent implements
		NetworkDataReceivedListener
{
	
	private static final String KEY_LANGUAGE = "language";
		
	private Spinner spSettingsChangeLanguage;
	private List<String> mLanguages = new ArrayList<>();
	
	@Override
	public void generateView(ViewGroup root) {
			mResource = R.layout.programming_setting_change_language;
			super.generateView(root);
			
			// get widgets
			spSettingsChangeLanguage = (Spinner) findViewById(R.id.spSettingsChangeLanguage);
			
			// connect to network data
			MainActivity.getInstance().addNetworkDataReceivedListener(this);
	}

	@Override
	public void updateSettings() {
		if( spSettingsChangeLanguage.getSelectedItem() != null ){
			mSettings.put( KEY_LANGUAGE, "\""
					+ spSettingsChangeLanguage.getSelectedItem() + "\"");
		}
	}

	@Override
	public void updateText(TextView txtText) {
		txtText.setText( (String) spSettingsChangeLanguage.getSelectedItem() );
	}

	@Override
	public void onNetworkDataReceived(DataResponsePackage data) {
		if( data.audioData != null && data.audioData.speechLanguagesList != null ){
			
			// generate languages list if new items available
			if( data.audioData.speechLanguagesList.length != mLanguages.size() ){
				mLanguages.clear();
				for( String lang : data.audioData.speechLanguagesList ){
					mLanguages.add( lang );
				}
				
				// add to spinner
				SpinnerAdapter vAdapter = new ArrayAdapter<>(
					getView().getContext(),
					android.R.layout.simple_spinner_item,
					mLanguages);
				spSettingsChangeLanguage.setAdapter(vAdapter);
			}
			
		}
	}

}
