package eu.edurob.nao.communicator.network;

import eu.edurob.nao.communicator.network.data.response.DataResponsePackage;
import eu.edurob.nao.communicator.network.interfaces.NetworkDataReceivedListener;

/**
 * Class of interface {@link Runnable} to notify a {@link NetworkDataReceivedListener}
 * @author Hannes Eilers
 *
 */
public class NetworkDataReceivedListenerNotifier implements Runnable{

	private DataResponsePackage data;
	private NetworkDataReceivedListener listener = null;
	
	public NetworkDataReceivedListenerNotifier(NetworkDataReceivedListener aListener, DataResponsePackage aData) {
		data = aData;
		listener = aListener;
	}
	
	@Override
	public void run() {
		listener.onNetworkDataReceived(data);
	}
	
}
