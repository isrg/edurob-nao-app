package eu.edurob.nao.communicator.network.interfaces;

import eu.edurob.nao.communicator.network.data.response.DataResponsePackage;

public interface NetworkDataSender {

	/**
	 * Adds a {@link NetworkDataReceivedListener} to this connector
	 * @param listener {@link NetworkDataReceivedListener} to add
	 */
	void addNetworkDataReceivedListener(NetworkDataReceivedListener listener);
	
	/**
	 * Removes a {@link NetworkDataReceivedListener} from this connector.
	 * If {@code listener} is {@code null} all listeners are removed.
	 * @param listener {@link NetworkDataReceivedListener} to remove or {@code null}
	 */
	void removeNetworkDataReceivedListener(NetworkDataReceivedListener listener);
	
	/**
	 * Notifies all listeners
	 * @param data
	 */
	void notifyDataReceivedListeners(DataResponsePackage data);
	
}
