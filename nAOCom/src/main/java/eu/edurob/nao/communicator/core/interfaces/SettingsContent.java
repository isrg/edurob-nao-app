package eu.edurob.nao.communicator.core.interfaces;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Map;

/**
 * Interface to get settings content
 * @author hannes
 *
 */
public interface SettingsContent {

	/**
	 * Functions to generate view and add listener.
	 */
	void generateView(ViewGroup root);
	
	/**
	 * Function to get settings view.
	 * @return	{@link View} of settings.
	 */
	View getView();
	
	/**
	 * Function to update settings {@link Map}.
	 */
	void updateSettings();
	
	/**
	 * Function to update content of {@link TextView}.
	 * @param txtText	{@link TextView} to update.
	 */
	void updateText(TextView txtText);
	
	/**
	 * Function to get Settings.
	 * @return	{@link Map} of {@link String} settings.
	 */
	Map<String, String> getSettings();
	
	/**
	 * @return JSON {@link String} of settings
	 */
	String toJson();
	
}
