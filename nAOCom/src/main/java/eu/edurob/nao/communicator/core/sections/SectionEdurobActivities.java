package eu.edurob.nao.communicator.core.sections;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import eu.edurob.nao.communicator.MainActivity;
import eu.edurob.nao.communicator.R;
import eu.edurob.nao.communicator.core.RemoteNAO;
import eu.edurob.nao.communicator.network.data.NAOCommands;

/**
 * Created by CMP3SHOPLNG on 24/10/2016.
 */
public class SectionEdurobActivities extends Section implements
        View.OnClickListener,
        View.OnFocusChangeListener,
        TextWatcher,
        AdapterView.OnItemSelectedListener
//        NetworkDataReceivedListener
//        SwipeRefreshLayout.OnRefreshListener
//        OnSeekBarChangeListener
{
    private static final String TAG = "SectionEdurobActivities"; //.class.getName();
    private static final int HISTORY_LENGTH = 10;
    private static final String SAVED_TEXT_FILE = "edurobTexts";
    private static final String PREFERENCES_SPEECH_RATE = "speechRate";
    private static final String PREFERENCES_SPEECH_MODULATION = "speechModulation";

    private EditText txtSpeechInputText;
    private Spinner lstSavedText;
    private ImageButton btnSpeechRemoveSavedText;
    private CheckBox chkSpeechAutomatic;
    private Button btnSayText;
    private Button btnSaveText;
    private LinearLayout lstSpeechHistory;
    private List<String> mHistory = new ArrayList<>();
    private List<String> mSavedText = new ArrayList<>();
    private ArrayAdapter<String> mSavedTextAdapter;

    private Button edurobBttnStand;
    private Button edurobBttnRest;
    private ImageButton edurobBttnTurnLeft;
    private ImageButton edurobBttnForward;
    private ImageButton edurobBttnTurnRight;
    private ImageButton edurobBttnStepLeft;
    private ImageButton edurobBttnBackward;
    private ImageButton edurobBttnStepRight;

    public SectionEdurobActivities(){
        super();
        this.title = MainActivity.getInstance().getResources().getString(R.string.section_edurob);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        rootView = inflater.inflate(R.layout.page_edurob, container, false);

        txtSpeechInputText = (EditText) findViewById(R.id.txtSpeechInputText);
        lstSavedText = (Spinner) findViewById(R.id.lstSavedText);
        btnSpeechRemoveSavedText = (ImageButton) findViewById(R.id.btnSpeechRemoveSavedText);
        chkSpeechAutomatic = (CheckBox) findViewById(R.id.chkSpeechAutomatic);
        btnSayText = (Button) findViewById(R.id.btnSayText);
        btnSaveText = (Button) findViewById(R.id.btnSaveText);
        lstSpeechHistory = (LinearLayout) findViewById(R.id.lstEdurobSpeechHistory);

        // set listener
        txtSpeechInputText.setOnFocusChangeListener(this);
        txtSpeechInputText.addTextChangedListener(this);

        // onclick listener
        btnSayText.setOnClickListener(this);
        btnSaveText.setOnClickListener(this);
        btnSpeechRemoveSavedText.setOnClickListener(this);

        // saved text adapter
        mSavedTextAdapter = new ArrayAdapter<>(getActivity(),
                                               R.layout.menu_list_item,
                                               mSavedText);
        lstSavedText.setAdapter(mSavedTextAdapter);
        lstSavedText.setOnItemSelectedListener(this);
        loadSavedTextsFromFile();

        (edurobBttnTurnLeft = (ImageButton) findViewById(R.id.edurobBttnTurnLeft)).setOnClickListener(this);
        (edurobBttnForward = (ImageButton) findViewById(R.id.edurobBttnForward)).setOnClickListener(this);
        (edurobBttnTurnRight = (ImageButton) findViewById(R.id.edurobBttnTurnRight)).setOnClickListener(this);
        (edurobBttnStepLeft = (ImageButton) findViewById(R.id.edurobBttnStepLeft)).setOnClickListener(this);
        (edurobBttnBackward = (ImageButton) findViewById(R.id.edurobBttnBackward)).setOnClickListener(this);
        (edurobBttnStepRight = (ImageButton) findViewById(R.id.edurobBttnStepRight)).setOnClickListener(this);
        (edurobBttnStand = (Button) findViewById(R.id.edurobBttnStand)).setOnClickListener(this);
        (edurobBttnRest = (Button) findViewById(R.id.edurobBttnRest)).setOnClickListener(this);

//        edurobActivityFunction = (LinearLayout) findViewById(R.id.divFunctionsCustom);

        TextView lblEdurobWalk = (TextView) findViewById(R.id.lblEdurobWalkTitle);
        LinearLayout divEdurobWalk = (LinearLayout) findViewById(R.id.divEdurobWalk);

//        // Register network data listener
//        MainActivity.getInstance().addNetworkDataReceivedListener(this);

        return rootView;
    }

    public void onTagClick(View v)
    {
        String tag = (String) v.getTag();

        if (!tag.isEmpty())
        {
            RemoteNAO.sendCommand(NAOCommands.MEMORY_EVENT_RAISE, new String[]{tag});
        }
    }

    @Override
    public void onClick(View v)
    {
        if (v == edurobBttnTurnLeft)
            RemoteNAO.sendCommand(NAOCommands.MEMORY_EVENT_RAISE, new String[]{"RemoteTurnLeft"});
        else if (v == edurobBttnForward)
            RemoteNAO.sendCommand(NAOCommands.MEMORY_EVENT_RAISE, new String[]{"RemoteForward"});
        else if (v == edurobBttnTurnRight)
            RemoteNAO.sendCommand(NAOCommands.MEMORY_EVENT_RAISE, new String[]{"RemoteTurnRight"});
        else if (v == edurobBttnStepLeft)
            RemoteNAO.sendCommand(NAOCommands.MEMORY_EVENT_RAISE, new String[]{"RemoteStepLeft"});
        else if (v == edurobBttnBackward)
            RemoteNAO.sendCommand(NAOCommands.MEMORY_EVENT_RAISE, new String[]{"RemoteBackward"});
        else if (v == edurobBttnStepRight)
            RemoteNAO.sendCommand(NAOCommands.MEMORY_EVENT_RAISE, new String[]{"RemoteStepRight"});
        else if (v == edurobBttnStand)
            RemoteNAO.sendCommand(NAOCommands.STAND_UP);
        else if (v == edurobBttnRest)
            RemoteNAO.sendCommand(NAOCommands.MEMORY_EVENT_RAISE, new String[]{"rest"});
//        else if (v == edurobBttnAnimalsQuiz)
//        {
//            RemoteNAO.sendCommand(NAOCommands.SET_LIFE_STATE, new String[]{"disabled"});
//            RemoteNAO.sendCommand(NAOCommands.MEMORY_EVENT_RAISE, new String[]{"ELS07_animals"});
//        }
//        else if (v == edurobBttnAnimalsAbort)
//        {
//            RemoteNAO.sendCommand(NAOCommands.MEMORY_EVENT_RAISE, new String[]{"naocomAbort"});
//            RemoteNAO.sendCommand(NAOCommands.SET_LIFE_STATE, new String[]{"solitary"});
//        }
//        else if (v == edurobBttnNumbersQuiz)
//        {
//            RemoteNAO.sendCommand(NAOCommands.SET_LIFE_STATE, new String[]{"disabled"});
//            RemoteNAO.sendCommand(NAOCommands.MEMORY_EVENT_RAISE, new String[]{"ELS07_numbers"});
//        }
//        else if (v == edurobBttnNumbersAbort)
//        {
//            RemoteNAO.sendCommand(NAOCommands.MEMORY_EVENT_RAISE, new String[]{"naocomAbort"});
//            RemoteNAO.sendCommand(NAOCommands.SET_LIFE_STATE, new String[]{"solitary"});
//        }
        else if( v == btnSayText )
        {

            // say text and add to history
            String vText = txtSpeechInputText.getText().toString().trim();

            // get values set on speech section
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            int speed = prefs.getInt(PREFERENCES_SPEECH_RATE, 100);
            int shape = prefs.getInt(PREFERENCES_SPEECH_MODULATION, 100);

            if( RemoteNAO.sendCommand( NAOCommands.SAY,
                                       new String[]{
                                               vText,
                                               Integer.toString(speed),
                                               Integer.toString(shape)} ) )
            {
                addToHistory(vText);
            }
        }
        else if( v == btnSaveText )
        {
            // save text
            String vText = txtSpeechInputText.getText().toString().trim();
            addSavedText( vText );
        }
        else if( v == btnSpeechRemoveSavedText )
        {
            // remove saved text item
            int position = lstSavedText.getSelectedItemPosition();
            removeFromSavedText(position);
        }
        else if( isInHistory(v) >= 0 )
        {
            int position = lstSpeechHistory.getChildCount() - 1 - isInHistory(v);
            txtSpeechInputText.setText( mHistory.get(position) );
        }
    }

    /**
     * Load saved texts from file
     */
    private void loadSavedTextsFromFile(){
        try {

            mSavedText.clear();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(getActivity().openFileInput(SAVED_TEXT_FILE) ) );
            mSavedText.add("");

            String line;
            while( (line = in.readLine()) != null ){
                mSavedText.add(line);
            }

            mSavedTextAdapter.notifyDataSetChanged();

        } catch (FileNotFoundException e) {
            Log.i(TAG, "File " + SAVED_TEXT_FILE + " with saved texts not found");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds a new text to history.
     * @param aText	{@link String} text.
     */
    private void addToHistory(String aText){
        aText = aText.trim();

        if( aText.length() > 0 ){

            // check if text is already in history > delete to bring to front
            if( isInHistory(aText) ){
                int position = mHistory.indexOf(aText);
                System.out.println("is in history: " + position);
                mHistory.remove(position);
                try{
                    lstSpeechHistory.removeViewAt( lstSpeechHistory.getChildCount() -1 - position );
                } catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            // add new TextView to first position in list
            TextView txt = new TextView(getActivity());
            txt.setText( aText );
            txt.setOnClickListener(this);
            txt.setTextAppearance(getActivity(), android.R.style.TextAppearance_Medium);
            mHistory.add( aText );
            lstSpeechHistory.addView( txt, 0 );

            while( mHistory.size() > HISTORY_LENGTH ){
                lstSpeechHistory.removeViewAt( lstSpeechHistory.getChildCount()-1 );
                mHistory.remove(0);
            }
        }
    }

    private void addSavedText( String aText){
        if( !isInSavedText(aText) ){
            // add text
            mSavedText.add(aText);
            saveText();
            mSavedTextAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Checks if a text is already in saved texts.
     * @param aText	{@link String} text.
     * @return		{@code true} if {@code aText} is in saved texts, {@code false} otherwise.
     */
    private boolean isInSavedText(String aText){
        for( String s : mSavedText ){
            if( s.equals(aText) ){
                return true;
            }
        }
        return false;
    }

    /**
     * Save text to file.
     */
    private void saveText(){
        try {

            // save to file
            FileOutputStream out = getActivity().openFileOutput(
                    SAVED_TEXT_FILE, Context.MODE_PRIVATE);
            for( String s : mSavedText ){
                if( s.length() > 0 ){
                    s += "\n";
                    out.write( s.getBytes() );
                }
            }
            out.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Remove entry from saved texts.
     * @param aPosition	{@link Integer} position of text in list.
     */
    private void removeFromSavedText(int aPosition){
        if( aPosition < mSavedText.size() && aPosition > 0 ){
            mSavedText.remove(aPosition);
            saveText();
            mSavedTextAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Checks if a text is already in history.
     * @param aText	{@link String} text.
     * @return		{@code true} if {@code aText} is in history, {@code false} otherwise.
     */
    private boolean isInHistory(String aText){
        for( String s : mHistory ){
            if( s.equals(aText) ){
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a {@link View} is in history list.
     * @param v		{@link View}
     * @return		{@link Integer} position of {@code v} in history,
     * 				{@code -1} if {@code v} is not in history.
     */
    private int isInHistory(View v){
        for( int i = 0; i < lstSpeechHistory.getChildCount(); i++ ){
            if( lstSpeechHistory.getChildAt(i) == v ){
                return i;
            }
        }
        return -1;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if( v == txtSpeechInputText && hasFocus ){
            txtSpeechInputText.setText("");
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}

    @Override
    public void afterTextChanged(Editable s) {
        if( chkSpeechAutomatic.isChecked() && s.length() > 0 ){
            char lastChar = s.charAt( s.length()-1 );

            // check if end of sentence > click say button
            if( lastChar == '.' || lastChar == '!' || lastChar == '?'
                    || ( lastChar == ' '
                    && s.length() > 1
                    && s.charAt(s.length()-2) == ' ' )){
                onClick(btnSayText);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if( parent == lstSavedText )
            txtSpeechInputText.setText( mSavedText.get(position) );
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {}
}
