package eu.edurob.nao.communicator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager.OnActivityResultListener;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import eu.edurob.nao.communicator.core.NAOComInstaller;
import eu.edurob.nao.communicator.core.RemoteNAO;
import eu.edurob.nao.communicator.core.SectionsPagerAdapter;
import eu.edurob.nao.communicator.core.revisions.ServerRevision;
import eu.edurob.nao.communicator.core.revisions.ServerRevisionChecker;
import eu.edurob.nao.communicator.core.sections.Section;
import eu.edurob.nao.communicator.core.sections.SectionConnect;
import eu.edurob.nao.communicator.core.sections.SectionEdurobActivities;
import eu.edurob.nao.communicator.core.sections.SectionFunctions;
import eu.edurob.nao.communicator.core.sections.SectionProgramming;
import eu.edurob.nao.communicator.core.sections.SectionSpeech;
import eu.edurob.nao.communicator.core.sections.SectionStatus;
import eu.edurob.nao.communicator.core.widgets.RemoteDevice;
import eu.edurob.nao.communicator.network.NetworkDataReceivedListenerNotifier;
import eu.edurob.nao.communicator.network.data.NAOCommands;
import eu.edurob.nao.communicator.network.data.response.DataResponsePackage;
import eu.edurob.nao.communicator.network.interfaces.NetworkDataReceivedListener;
import eu.edurob.nao.communicator.network.interfaces.NetworkDataSender;

public class MainActivity extends FragmentActivity implements
		NetworkDataReceivedListener,
		NetworkDataSender,
		OnItemClickListener{

	public static final String INSTALLER_INTENT_EXTRA_WORKSTATION = "installer.intent.extra.device";
	public static final String INSTALLER_INTENT_EXTRA_HOST = "installer.intent.extra.host";
	public static final String INSTALLER_INTENT_EXTRA_REVISION = "installer.intent.extra.revision";
	public static final String INSTALLER_INTENT_EXTRA_UPDATE = "installer.intent.extra.update";

	private static MainActivity INSTANCE;
	private static final String SHARED_PREFERENCES = "naocom_preferences";
	private static final String INSTANCE_STATE_KEY_HOST_ADDRESSES = "HOSTS";
	private static ServerRevision onlineRevision;

	private List<Section> mSections = new ArrayList<>();
	private RemoteDevice mConnectedDevice = null;
	private List<NetworkDataReceivedListener> dataReceivedListener = new ArrayList<>();
	private List<OnActivityResultListener> activityResultListener = new ArrayList<>();

	private String mTitle;
	private SectionsPagerAdapter mSectionsPagerAdapter;
	private ViewPager mViewPager;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private LinearLayout mMenu;
	private ListView mMenuListView;
	private String[] mMenuItems;

	/**
	 * Called if activity created
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		INSTANCE = this;

		setContentView(R.layout.activity_main);
		updateTitleAsOffline();

		// add layouts
		createPageFragmentLayouts();

		// Create the adapter that will return a fragment for each of
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// set up left slide menu
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerLayout.setDrawerShadow( R.drawable.drawer_shadow, GravityCompat.START );
		mMenu = (LinearLayout) findViewById(R.id.menu);
		mMenuListView = (ListView) findViewById(R.id.lstMenu);
		mMenuItems = getResources().getStringArray(R.array.section_names);
		mMenuListView.setAdapter(new ArrayAdapter<>(this, R.layout.menu_list_item, mMenuItems));

		mMenuListView.setOnItemClickListener(this);
		mMenuListView.setItemChecked(0, true);

		// enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,					/* host Activity */
                mDrawerLayout,			/* DrawerLayout object */
                R.drawable.ic_drawer,	/* nav drawer image to replace 'Up' caret */
                R.string.menu_open,		/* "open drawer" description for accessibility */
                R.string.menu_close		/* "close drawer" description for accessibility */
                ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // start fetching data for server update
        (new Thread(new ServerRevisionChecker())).start();

	}

	@Override
	protected void onActivityResult(int requestCode,int resultCode,Intent data) {
		// reconnect remote nao
		RemoteNAO vRemoteNao = RemoteNAO.getCurrentRemoteNao();
		if( vRemoteNao != null && !vRemoteNao.isConnected() ){
			vRemoteNao.reconnect();
		}

		// call listener
		for( OnActivityResultListener listener : activityResultListener ){
			listener.onActivityResult(requestCode, resultCode, data);
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * Adds layouts for fragment pages
	 */
	private void createPageFragmentLayouts(){
		if( mSections.size() == 0 ){
			// Add all new sections here
			mSections.add( new SectionConnect() );
//			mSections.add( new SectionHotspot() );
			mSections.add( new SectionStatus() );
			mSections.add( new SectionSpeech() );
			mSections.add( new SectionFunctions() );
			mSections.add( new SectionEdurobActivities());
//			mSections.add( new SectionLed() );
			mSections.add( new SectionProgramming() );
		}
	}

	/**
	 * @return the sections
	 */
	public List<Section> getSections() {
		return mSections;
	}

	/**
	 * @return the connectedDevice
	 */
	public RemoteDevice getConnectedDevice() {
		return mConnectedDevice;
	}

	/**
	 * @param aConnectedDevice the connectedDevice to set
	 */
	public void setConnectedDevice(RemoteDevice aConnectedDevice) {
		if( mConnectedDevice != null ){
			mConnectedDevice.getNao().removeNetworkDataReceivedListener(this);
		}
		mConnectedDevice = aConnectedDevice;

		if( mConnectedDevice != null ){
			mConnectedDevice.getNao().addNetworkDataReceivedListener(this);
		}
	}

	/**
	 * Update action bar title
	 * @param aTitle	{@link String} for new title
	 */
	public void updateTitle(String aTitle){
		mTitle = aTitle;
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override
			public void run() {
				setTitle( mTitle );
			}
		});
	}

	/**
	 * @return Current {@link ViewPager}
	 */
	public ViewPager getViewPager() {
		return mViewPager;
	}

	/**
	 * Function called if a section header is clicked.
	 * Expands / Collapses the following {@link LinearLayout}, if existing.
	 * @param vView		Section header {@link View}
	 */
	public void containerClicked(View vView){

		ViewGroup vParent = (ViewGroup) vView.getParent();
		if( vParent != null ){

			// get next view
			int vPosition = vParent.indexOfChild(vView)+1;
			if( vParent.getChildCount() > vPosition ){
				View vViewContainer = vParent.getChildAt(vPosition);
				if( vViewContainer != null
						&& vViewContainer instanceof LinearLayout
						&& vView instanceof TextView){

					// cast text view
					TextView vTextView = (TextView) vView;
					if( vViewContainer.getVisibility() == View.VISIBLE ){

						// set visible
						vViewContainer.setVisibility( View.GONE );
						vTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0,
								R.drawable.ic_action_expand, 0);

					} else {

						// set invisible
						vViewContainer.setVisibility( View.VISIBLE );
						vTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0,
								R.drawable.ic_action_collapse, 0);

					}
				}
			}
		}
	}

	/**
	 * Adds an external {@link OnActivityResultListener}.
	 * @param listener	{@link OnActivityResultListener} to add.
	 */
	public void addOnActivityResultListener(OnActivityResultListener listener){
		// add listener
		if( !activityResultListener.contains(listener) ){
			activityResultListener.add(listener);
		}
	}

	/**
	 * Removes an external {@link OnActivityResultListener}.
	 * @param listener	{@link OnActivityResultListener} to remove.
	 */
	public void removeOnActivityResultListener(OnActivityResultListener listener){
		activityResultListener.remove(listener);
	}

	/**
	 * Selects item in menu drawer
	 * @param section
	 */
	public void selectMenuItem(Section section){
		for( int i=0; i < mSections.size(); i++ ){
			Section s = mSections.get(i);
			if( s == section ){
				mMenuListView.setItemChecked(i, true);
			}
		}
	}

	/**
	 * @return	{@link Integer} of online available server revision.
	 */
	public ServerRevision getOnlineRevision(){
		return onlineRevision;
	}

	/**
	 * Sets revision of online available server.
	 * @param aRevision	{@link ServerRevision} revision.
	 */
	public void setOnlineRevision(ServerRevision aRevision){
		if( aRevision == null ){
			aRevision = new ServerRevision();
		}
		onlineRevision = aRevision;
	}

	/**
	 * Starts the installer to update or install the latest server revision on remote device.
	 * @param aDevice		{@link RemoteDevice} to install server.
	 * @param aUpdate		{@link Boolean} update flag. {@code true} if to update existing server, {@code false} otherwise.
	 */
	public void startInstaller(RemoteDevice aDevice, boolean aUpdate){

		ServerRevision vRevision = getOnlineRevision();

		if( vRevision.getRevision() >= 0 ){
			// create intent
			Intent vIntent = new Intent(this, NAOComInstaller.class);
			Gson vGson = new Gson();
			String jsonRevision = vGson.toJson(vRevision);

			vIntent.putExtra( INSTALLER_INTENT_EXTRA_WORKSTATION, aDevice.getWorkstationName() );
			vIntent.putExtra( INSTALLER_INTENT_EXTRA_HOST, aDevice.getNao().getHostAddresses().get(0));
			vIntent.putExtra( INSTALLER_INTENT_EXTRA_REVISION, jsonRevision );
			vIntent.putExtra( INSTALLER_INTENT_EXTRA_UPDATE, aUpdate );

			// disconnect
			if( aDevice != null ){
				aDevice.getNao().disconnect();
			}

			// start installer activity
			startActivity(vIntent);
		}
	}

	/**
	 * @return Current {@link MainActivity} instance.
	 */
	public static MainActivity getInstance() {
		return INSTANCE;
	}

	/**
	 * @return	Applications {@link SharedPreferences}.
	 */
	public static SharedPreferences getPreferences(){
		return getInstance().getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
	}

    public void updateTitleAsOffline()
    {
        String status = getResources().getString(R.string.status_offline);
        String appName = getResources().getString(R.string.app_name);
		updateTitle( "[" + status + "] " + appName );
	}

	@Override
	public void onNetworkDataReceived(DataResponsePackage data) {
		if( data.request.command == NAOCommands.SYS_DISCONNECT && data.requestSuccessfull){
			updateTitleAsOffline();
		} else {
			updateTitle( "[" + data.batteryLevel + "%] " + data.naoName );
		}
		notifyDataReceivedListeners(data);
	}

	@Override
	protected void onStop() {
		RemoteNAO nao = RemoteNAO.getCurrentRemoteNao();
		if( nao != null ){
			nao.disconnect();
		}
		super.onStop();
	}

	@Override
	public void addNetworkDataReceivedListener(NetworkDataReceivedListener listener){
		synchronized(dataReceivedListener) {
			dataReceivedListener.add(listener);
		}
	}

	@Override
	public void removeNetworkDataReceivedListener(NetworkDataReceivedListener listener){
		synchronized(dataReceivedListener) {
			if( listener == null ){
				dataReceivedListener.clear();
			}
			else{
				dataReceivedListener.remove(listener);
			}
		}
	}

	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


	@Override
	public void notifyDataReceivedListeners(DataResponsePackage data){
		synchronized (dataReceivedListener) {
			for( NetworkDataReceivedListener listener : dataReceivedListener){
				Runnable r = new NetworkDataReceivedListenerNotifier(listener, data);
				new Thread(r).start();
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if( mDrawerToggle.onOptionsItemSelected(item) ){
			// menu item clicked
			if( mDrawerLayout.isDrawerOpen(mMenu) ){
				mDrawerLayout.closeDrawer(mMenu);
			} else {
				mDrawerLayout.openDrawer(mMenu);
			}
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		mViewPager.setCurrentItem(position);
		mMenuListView.setItemChecked(position, true);
		mDrawerLayout.closeDrawer(mMenu);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// check if device connected
		RemoteDevice vDevice = getConnectedDevice();
		if( vDevice != null && vDevice.getNao().isConnected() ){

			// add connected hosts
			ArrayList<String> vHostAddresses = new ArrayList<>( vDevice.getNao().getHostAddresses() );
			outState.putStringArrayList(INSTANCE_STATE_KEY_HOST_ADDRESSES, vHostAddresses);

		}
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// check if device was connected
		if( savedInstanceState.containsKey(INSTANCE_STATE_KEY_HOST_ADDRESSES) ){

			ArrayList<String> vHostAddresses = savedInstanceState.getStringArrayList(INSTANCE_STATE_KEY_HOST_ADDRESSES);
			if( vHostAddresses.size() > 0 ){

				// create new remote device and add host addresses
				RemoteDevice vDevice = new RemoteDevice(this, vHostAddresses.get(0));
				for( String vHost : vHostAddresses ){
					vDevice.addAddress(vHost);
				}

				// add remote device
				SectionConnect.getInstance().clearDevicesList();
				SectionConnect.getInstance().addRemoteDevice(vDevice);

				// connect device
				vDevice.connect();

			}
		}
		super.onRestoreInstanceState(savedInstanceState);
	}

	public void onTagClick(View v)
    {
        String tag = (String) v.getTag();

        if (!tag.isEmpty())
        {
            RemoteNAO.sendCommand(NAOCommands.MEMORY_EVENT_RAISE, new String[]{tag});
        }
    }
}
