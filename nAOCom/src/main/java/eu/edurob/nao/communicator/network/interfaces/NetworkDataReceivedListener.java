package eu.edurob.nao.communicator.network.interfaces;

import eu.edurob.nao.communicator.network.data.response.DataResponsePackage;

public interface NetworkDataReceivedListener {

	void onNetworkDataReceived(DataResponsePackage data);
	
}
