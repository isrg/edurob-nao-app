package eu.edurob.nao.communicator.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jmdns.ServiceEvent;

import eu.edurob.nao.communicator.MainActivity;
import eu.edurob.nao.communicator.core.interfaces.NAOInterface;
import eu.edurob.nao.communicator.core.widgets.RemoteDevice;
import eu.edurob.nao.communicator.network.ConnectionState;
import eu.edurob.nao.communicator.network.NAOConnector;
import eu.edurob.nao.communicator.network.NetworkDataReceivedListenerNotifier;
import eu.edurob.nao.communicator.network.data.NAOCommands;
import eu.edurob.nao.communicator.network.data.response.DataResponsePackage;
import eu.edurob.nao.communicator.network.interfaces.NetworkDataReceivedListener;
import eu.edurob.nao.communicator.network.interfaces.NetworkDataSender;
import eu.edurob.nao.communicator.network.interfaces.NetworkServiceHandler;

/**
 * Class for handle connection to remote nao
 * and call functions
 * @author Hannes Eilers
 *
 */
public class RemoteNAO implements NAOInterface, NetworkDataSender, NetworkDataReceivedListener, NetworkServiceHandler {

	public static final String naoNetworkServiceToken = "_nao._tcp.local.";
	public static final String naoqiNetworkServiceToken = "_naoqi._tcp.local.";
	public static final String sshNetworkServiceToken = "_ssh._tcp.local.";
	public static final String sftpNetworkServiceToken = "_sftp-ssh._tcp.local.";
	
	
	private NAOConnector connector = null;
	private Map<String, Boolean> services = new HashMap<>();
	private String name = null;
	private List<NetworkDataReceivedListener> dataReceivedListener = new ArrayList<>();
	
	public RemoteNAO() {
	}
	
	/**
	 * Constructor using {@link ServiceEvent} to set remote device information
	 * @param service
	 */
	public RemoteNAO(ServiceEvent service){
		addNetworkService(service);
	}
	
	/**
	 * Constructor to set remote device
	 * @param host
	 * @param port
	 */
	public RemoteNAO(String host, int port) {
		connector = new NAOConnector(host, port);
	}
	
	/**
	 * @return	{@code true} if remote NAO is connected, {@code false} otherwise.
	 */
	public boolean isConnected(){
		return connector != null
				&& connector.getConnectionState() == ConnectionState.CONNECTION_ESTABLISHED;

	}
	
	@Override
	public void addNetworkDataReceivedListener(NetworkDataReceivedListener listener){
		dataReceivedListener.add(listener);
	}
	
	@Override
	public void removeNetworkDataReceivedListener(NetworkDataReceivedListener listener){
		if( listener == null ){
			dataReceivedListener.clear();
		}
		else{
			dataReceivedListener.remove(listener);
		}
	}
	
	@Override
	public void notifyDataReceivedListeners(DataResponsePackage data){
		for( NetworkDataReceivedListener listener : dataReceivedListener){
			Runnable r = new NetworkDataReceivedListenerNotifier(listener, data);
			new Thread(r).start();
		}
	}
	
	@Override
	public boolean connect(){
		if( connector != null ){
			
			if( connector.getConnectionState() != ConnectionState.CONNECTION_INIT ){

				disconnect();
				connector = new NAOConnector(connector);
				
			} 
			
			connector.addNetworkDataReceivedListener(this);
			connector.start();			
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean reconnect() {		
		if( connector != null ){
			// disconnect existing connection
			disconnect();
			
			// reset connector
			connector = new NAOConnector(connector);
		}	
		
		// connect and return result
		return connect();
	}
	
	@Override
	public void disconnect(){
		if( connector != null ){
			connector.removeNetworkDataReceivedListener(this);
			connector.stopConnector();
		}
	}

	@Override
	public void onNetworkDataReceived(DataResponsePackage data) {
		notifyDataReceivedListeners(data);
		
		if( data.request.command == NAOCommands.SYS_DISCONNECT
				&& data.requestSuccessfull){
			MainActivity.getInstance().updateTitleAsOffline();
		}
	}

	@Override
	public void addNetworkService(ServiceEvent service) {
		String serviceType = service.getType();
		
		synchronized (services) {
			services.put(serviceType, true);
		}		
		
		// check connector and add host addresses
		if(connector == null){
			connector = new NAOConnector(service);
		}
		
		connector.addHostAddresses(service.getInfo().getHostAddresses());
		
		// check for communication server or only nao
		if( serviceType.contains(NAOConnector.serverNetworkServiceToken) ){
			name = service.getName();
		}		
	}

	@Override
	public void removeNetworkService(ServiceEvent service) {
		synchronized (services) {
			services.remove(service.getType());
		}
	}

	@Override
	public List<String> getHostAddresses() {
		if( connector != null ){
			return connector.getHostAddresses();
		}
		return new ArrayList<>();
	}
	
	@Override
	public void addHostAddress(String aAddress) {
		if( connector != null ){
			connector.addHostAddress(aAddress);
		} else {
			connector = new NAOConnector(aAddress, NAOConnector.defaultPort);
		}
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean hasNAOqi() {
		return getServiceStatus(naoqiNetworkServiceToken);
	}

	@Override
	public boolean hasSSH() {
		return getServiceStatus(sshNetworkServiceToken);
	}

	@Override
	public boolean hasSFTP() {
		return getServiceStatus(sftpNetworkServiceToken);
	}
	
	@Override
	public boolean isNAO() {
		return getServiceStatus(naoNetworkServiceToken);
	}
	
	@Override
	public boolean hasCommunicationServer() {
		return getServiceStatus( NAOConnector.serverNetworkServiceToken );
	}
	
	/**
	 * @param serviceToken
	 * @return Status of the service
	 */
	private boolean getServiceStatus(String serviceToken){
		synchronized (services) {
			for( String key : services.keySet() ){
				if( key.contains(serviceToken) ){
					return services.get(key);
				}
			}
		}		
		
		return false;
	}
	
	/**
	 * @return	Currently connected {@link RemoteNAO} or {@code null} if none connected.
	 */
	public static synchronized RemoteNAO getCurrentRemoteNao(){
		RemoteDevice vRemoteDevice = MainActivity.getInstance().getConnectedDevice();
		if( vRemoteDevice != null
				&& vRemoteDevice.getNao() != null ){
			return vRemoteDevice.getNao();
		}
		
		return null;
	}
	
	/**
	 * Send {@link NAOCommands} to remote NAO.
	 * @param aCommand	{@link NAOCommands} to send.
	 * @param aArgs		Array of {@link String} arguments to for {@code aCommand}.
	 * @return			{@code true} if successful, {@code false} otherwise.
	 */
	public static boolean sendCommand(NAOCommands aCommand, String[] aArgs){
		RemoteNAO nao = getCurrentRemoteNao();
		if( nao != null ){
			return nao.connector.sendCommand(aCommand, aArgs);
		}
		
		return false;
	}
	
	/**
	 * Send {@link NAOCommands} to remote NAO.
	 * @param aCommand	{@link NAOCommands} to send.
	 * @return			{@code true} if successful, {@code false} otherwise.
	 */
	public static boolean sendCommand(NAOCommands aCommand){
		return sendCommand(aCommand, new String[]{});
	}

	/**
	 * @return {@link NAOConnector} of this {@link RemoteNAO}.
	 */
	public NAOConnector getConnector() {
		return connector;
	}
	
}
