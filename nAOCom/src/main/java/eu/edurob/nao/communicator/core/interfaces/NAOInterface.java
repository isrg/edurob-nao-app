package eu.edurob.nao.communicator.core.interfaces;

import java.util.List;

public interface NAOInterface {

	/**
	 * Connects to NAO
	 * @return {@code true} if connecting process started, {@code false} otherwise
	 */
	boolean connect();
	
	/**
	 * Reconnects to NAO and replaces existing connection.
	 * @return {@code true} if connecting process started, {@code false} otherwise
	 */
	boolean reconnect();
	
	/**
	 * Disconnect from NAO
	 */
	void disconnect();
	
	/**
	 * @return Name of NAO
	 */
	String getName();
	
	/**
	 * @return {@code true} if NAO has naoqi running, {@code false} otherwise
	 */
	boolean hasNAOqi();
	
	/**
	 * @return {@code true} if NAO has ssh server running, {@code false} otherwise
	 */
	boolean hasSSH();
	
	/**
	 * @return {@code true} if NAO has sftp server running, {@code false} otherwise
	 */
	boolean hasSFTP();
	
	/**
	 * @return {@code true} if NAO has nao network service running, {@code false} otherwise
	 */
	boolean isNAO();
	
	/**
	 * @return {@code true} if NAO has nao communication server running, {@code false} otherwise
	 */
	boolean hasCommunicationServer();
	
	/**
	 * @return Address of remote host
	 */
	List<String> getHostAddresses();
	
	/**
	 * Add new host address
	 * @param aAddress	{@link String} host address
	 */
	void addHostAddress(String aAddress);
	
}
