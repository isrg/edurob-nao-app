# EDUROB NAO Controller #
This is the Android code for the EDUROB ([edurob.eu](http://edurob.eu)) project's NAO Controller app. It allows simple control of a NAO robot.

### What is this repository for? ###
* This repository hosts the development code for the [EDUROB RobotController app](https://play.google.com/store/apps/details?id=eu.edurob.nao.communicator)
* The current release version is 0.2.1

### How do I get set up? ###

#### Prerequisites ####
* [Android Studio](https://developer.android.com/studio/) (You only need the SDK tools package, not Android Studio)
* [Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/) - (You'd probably be OK with [Open JDK](http://openjdk.java.net) on Linux)

#### Additional Robot Program Files Used in the Project ####
* NAO Choregraphe files http://isrg.org.uk/robot/

# CREDITS #
This work is based on source code from the [NAO Com](https://github.com/NorthernStars/NAO-Com) Android app developed by [Northern Stars](http://www.northernstars.de).

This project (543577-LLP-1-2013-1-UK-KA3-KA3MP) has been funded with support from the European Commission.
This publication reflects the views only of the author, and the Commission cannot be held responsible for any use which may be made of the information contained therein.

# LICENSING #
![88x31.png](https://bitbucket.org/repo/M4EqLB/images/1763262374-88x31.png)

The Edurob NAO Controller by The Edurob Project Consortium is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License. 
Based on a work at https://github.com/NorthernStars/NAO-Com.

Permissions beyond the scope of this license may be available at http://edurob.eu.